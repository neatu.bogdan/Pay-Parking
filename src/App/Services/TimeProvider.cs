﻿using App.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services
{
    public class TimeProvider : ITimeProvider
    {
        private DateTime _time;

        public TimeProvider()
        {
            _time = DateTime.Now; 
        }

        public DateTime GetTime()
        {
            return _time;
        }

        public void AddHours(int count)
        {
            _time = _time.AddHours(count);
        }
        
        public void AddMinutes(int count)
        {
            _time = _time.AddMinutes(count); 
        }

        public void AddSeconds(int count)
        {
            _time = _time.AddSeconds(count); 
        }
    }
}
