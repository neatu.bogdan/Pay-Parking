﻿using App.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services
{
    public class BillingService : IBillingService
    {
        public int FirstHourPrice { get; private set; }
        public int HourlyPrice { get; private set; }
        public BillingService(int firstHourPrice, int hourlyPrice)
        {
            FirstHourPrice = firstHourPrice;
            HourlyPrice = hourlyPrice; 
        }

        public int CalculateBill(DateTime start, DateTime end)
        {
            double totalTime = end.Subtract(start).TotalHours;
            if (totalTime == 0)
                return 0;
            int totalPrice = FirstHourPrice + (int)(Math.Ceiling(totalTime)-1)*HourlyPrice;

            return totalPrice;
        }
       
    }
}
