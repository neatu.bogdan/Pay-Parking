﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Entities
{
    /// <summary>
    /// This represents a parking summary for a specific car 
    /// </summary>
    public class Summary 
    {
        public DateTime EntryTime { get; private set; }
        public DateTime ExitTime { get; private set; }
        public int TotalPrice { get; private set; }
        public Car Car { get; set; }

        public Summary(DateTime entryTime, DateTime exitTime, int totalPrice, Car car)
        {
            EntryTime = entryTime;
            ExitTime = exitTime;
            TotalPrice = totalPrice;
            Car = car; 
        }
        public string ToJSON()
        {
            string serializedSummary = JsonConvert.SerializeObject(this, Formatting.Indented);
            return serializedSummary; 
        }

    }
}
