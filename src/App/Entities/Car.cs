﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace App.Entities
{
    public class Car 
    {
        public string LicensePlate { get; set; }

        public Car(string licensePlate)
        {
            LicensePlate = licensePlate;
        }
    }

    /// <summary>
    /// Equality comparer class which allows to use Car as key in a dictionary
    /// </summary>
    public class CarEqualityComparer : IEqualityComparer<Car>
    {
        public bool Equals(Car x, Car y)
        {
            return x.LicensePlate == y.LicensePlate;
        }

        public int GetHashCode(Car car)
        {
            return car.LicensePlate.GetHashCode();
        }
    }
}
