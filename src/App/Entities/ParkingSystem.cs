﻿using App.Exceptions;
using App.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace App.Entities
{
    public class ParkingSystem
    {
        public int TotalSpaces { get; set; }
        public IDictionary<Car,DateTime> ParkedCars { get; set; }
        public int FreeSpaces { 
            get => TotalSpaces - ParkedCars.Count; 
        }
        public ITimeProvider TimeProvider { get; set; }
        public IBillingService BillingService { get; set; }

        public ParkingSystem(ITimeProvider timeProvider, IBillingService billingSystem)
        {
            ParkedCars = new Dictionary<Car, DateTime>(new CarEqualityComparer());
            BillingService = billingSystem;
            TimeProvider = timeProvider;
            TotalSpaces = 10; 
        }

        public void CarCheckin(Car car)
        {
            if (ParkedCars.ContainsKey(car))
                throw new CarAlreadyCheckedInException(car.LicensePlate);
            if (FreeSpaces > 0)
                ParkedCars.Add(car, TimeProvider.GetTime());
            else
                throw new FullParkingLotException();
            
        }

        public int CalculateBill(Car car)
        {
            if (!ParkedCars.Keys.Contains(car))
                throw new CarNotFoundException(car.LicensePlate); 

            var startTime = ParkedCars[car];
            var endTime = TimeProvider.GetTime();
            
            return BillingService.CalculateBill(startTime, endTime); 
        }

        /// <summary>
        /// Method which checksout a car only if money supplied are enough to pay the bill
        /// </summary>
        public Summary CarCheckout(Car car, int moneySupplied)
        {
            if (!ParkedCars.Keys.Contains(car))
                throw new CarNotFoundException(car.LicensePlate);

            var startTime = ParkedCars[car];
            var endTime = TimeProvider.GetTime();
            var totalToPay = BillingService.CalculateBill(startTime, endTime);

            if (totalToPay > moneySupplied)
                throw new NotEnoughMoneySuppliedException(totalToPay);
            
            ParkedCars.Remove(car);
            return new Summary(startTime, endTime, totalToPay, car);
        }

        public string GetParkedCars()
        {
            Dictionary<int, Car> parkedCars = new Dictionary<int, Car>();
            var count = 1;
            foreach (var carMapping in ParkedCars)
                parkedCars.Add(count++, carMapping.Key);

            return JsonConvert.SerializeObject(
                new 
                {
                    totalSpaces = TotalSpaces,
                    freeSpaces = TotalSpaces - count + 1,
                    parkedCars = parkedCars,
                },
                Formatting.Indented);
        }
    }
}
