﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Interfaces
{
    public interface IBillingService
    {
        public int CalculateBill(DateTime start, DateTime end); 
    }
}
