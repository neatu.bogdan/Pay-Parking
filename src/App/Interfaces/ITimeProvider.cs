﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Interfaces
{
    public interface ITimeProvider
    {
        public DateTime GetTime();
    }
}
