﻿using App.Entities;
using App.Interfaces;
using App.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;

namespace App
{
    class Program
    {
        static void Main(string[] args)
        {

            CLIApplication.Init();
            CLIApplication.Run();
        }
        
    }
}

/*
        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureServices(services =>
                   {
                       services.AddTransient<ITimeProvider, TimeProvider>();
                       services.AddTransient<IBillingService>( s => new BillingService(10,5));
                       services.AddSingleton<ParkingSystem>(); 
                   }
                );
        }
        */
