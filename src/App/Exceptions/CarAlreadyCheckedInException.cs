﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Exceptions
{
    public class CarAlreadyCheckedInException : Exception
    {
        public CarAlreadyCheckedInException(string licensePlate):
            base($"Car with license plate {licensePlate} already exists in the parking lot.")
        {

        }
    }
}
