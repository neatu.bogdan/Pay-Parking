﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Exceptions
{
    public class FullParkingLotException : Exception
    {
        public FullParkingLotException()
            :base("There are no empty parking spaces!")
        {

        }
    }
}
