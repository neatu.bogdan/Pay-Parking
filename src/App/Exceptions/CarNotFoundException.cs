﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Exceptions
{
    public class CarNotFoundException : Exception
    {
        public CarNotFoundException(string licensePlate)
            :base ($"Car with plate number {licensePlate} was not found in the parking lot.")
        {

        }
    }
}
