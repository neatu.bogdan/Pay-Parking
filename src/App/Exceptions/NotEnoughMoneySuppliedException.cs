﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Exceptions
{
    public class NotEnoughMoneySuppliedException : Exception
    {
        public NotEnoughMoneySuppliedException(int totalToPay):
            base($"You have not supplied enough money. Please try again with {totalToPay}")
        {

        }
    }
}
