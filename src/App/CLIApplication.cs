﻿using App.Entities;
using App.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace App
{
    public class CLIApplication
    {
        private static TimeProvider _timeProvider;
        private static BillingService _billingService;
        private static ParkingSystem _parkingSystem;

        public static void Init()
        {
            _timeProvider = new TimeProvider();
            _billingService = new BillingService(10, 5);
            _parkingSystem = new ParkingSystem(_timeProvider, _billingService);
        }

        public static void DisplayMenu()
        {
            Console.WriteLine("APPLICATION USAGE: \n" +
                              "   bill <LICENSE PLATE>                            Calculates bill for a car\n" +
                              "   checkin <LICENSE PLATE>                         Checks in a new car\n" +
                              "   checkout <LICENSE PLATE> <AMOUNT OF MONEY>      Checks out the car and displays summary in JSON format\n" +
                              "   listcars                                        Displays all cars in the parking lot in JSON format\n" +
                              "   addminutes <NUMBER>                             Increments parking system's time with <NUMBER> minutes\n" +
                              "   addhours <NUMBER>                               Increments parking system's time with <NUMBER> HOURS\n" +
                              "   freespaces                                      Displays the number of free spaces in the parking lot\n" +
                              "   help                                            Displays APPLICATION USAGE instuctions\n" +
                              "   exit                                            Shuts down the system\n"
                              );
        }

        /// <summary>
        /// This method contains a loop that runs forever and handles CLI commands 
        /// </summary>
        public static void Run()
        {
            DisplayMenu();
            Console.Write("> ");
            var command = Console.ReadLine();

            while (!command.Equals("exit"))
            {
                try
                {
                    if (command.StartsWith("checkin"))
                    {
                        Checkin(command);
                        continue;
                    }
                    if (command.StartsWith("checkout"))
                    {
                        var summary = Checkout(command);
                        Console.WriteLine("\n Summary :\n\n" + summary.ToJSON());
                        continue;
                    }
                    if (command.StartsWith("bill"))
                    {
                        var amountToPay = Bill(command);
                        Console.WriteLine($"You have to pay {amountToPay} lei.");
                        continue;
                    }
                    if (command.StartsWith("addminutes"))
                    {
                        var minutes = Convert.ToInt32(command.Split(" ")[1]);
                        _timeProvider.AddMinutes(minutes);
                        continue;
                    }
                    if (command.StartsWith("addhours"))
                    {
                        var hours = Convert.ToInt32(command.Split(" ")[1]);
                        _timeProvider.AddHours(hours);
                        continue;
                    }

                    switch (command)
                    {
                        case "freespaces":
                            Console.WriteLine($"There are {_parkingSystem.FreeSpaces} free spaces in the parking lot.");
                            break;
                        case "help":
                            DisplayMenu();
                            break;
                        case "listcars":
                            Console.WriteLine(_parkingSystem.GetParkedCars());
                            break;
                        case "":
                            break;
                        default:
                            Console.WriteLine("Unknown command");
                            break;
                    }

                }
                catch (Exception e)
                {
                    if (e.Message == "Index was outside the bounds of the array.")
                        DisplayMenu();
                    else
                        Console.WriteLine(e.Message);
                }
                finally
                {
                    Console.Write("> ");
                    command = Console.ReadLine();
                }

            }
        }

        private static void Checkin(string command)
        {
            var licensePlate = command.Split(" ")[1];
            var newCar = new Car(licensePlate);
            _parkingSystem.CarCheckin(newCar);
        }
        private static Summary Checkout(string command)
        {
            var licensePlate = command.Split(" ")[1];
            var money = Convert.ToInt32(command.Split(" ")[2]);
            var car = new Car(licensePlate);
            var summary = _parkingSystem.CarCheckout(car, money);
            return summary;
        }
        private static int Bill(string command)
        {
            var licensePlate = command.Split(" ")[1];
            var car = new Car(licensePlate);
            return _parkingSystem.CalculateBill(car);
        }
    }
}
