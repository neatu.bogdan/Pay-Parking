# Pay Parking

This is a simple CLI application which simulates a Parking System and has the follwing features:  

---
### 1. Bill for specific Car

This command queries the system regarding the amount of money a driver has to pay in order to exit the parking lot  

Syntax :  

    >bill  
---
### 2. Car check-in 
This features simulates the entrance of car in the parking lot. The license plate and the time are registered  

Syntax :  

    >checkin <LICENSE PLATE>  
---
### 3. Car check-out
This command is called when a driver wants to exit the parking lot. He must pay an amount of money acording to how much time the car spent in the parking lot  

Syntax :  

    >checkout <LICENSE PLATE> <AMOUNT OF MONEY>
---
### 4. List Cars
This command queries the system and displays parked Cars  

Syntax :  

    >listcars
---
### 5. Add minutes to system clock
It increments system clock with the specified number of minutes  

Syntax :  

    >addminutes <NUMBER>
---
### 6. Add hours to system clock
It increments system clock with the specified number of hours  

Syntax :  

    >addhours <NUMBER>
---
### 7. Display Free spaces  

Syntax :  

    >freespaces

