﻿using App.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUnitTests.Services
{
    [TestClass]
    public class BillingSystemTests
    {
        private readonly BillingService _billingSystem;
        private readonly int _firstHourPrice = 10;
        private readonly int _hourlyPrice = 5;

        public BillingSystemTests()
        {
            _billingSystem = new BillingService(_firstHourPrice,_hourlyPrice); 
        }

        [TestMethod]
        public void ShouldCalculateBillAccordingToPrices()
        {
            DateTime start = DateTime.Now;
            DateTime end = start.AddHours(1);
            end = end.AddMinutes(1);
            int totalToPay = _billingSystem.CalculateBill(start, end);

            //The total should be the price of two hours, first charged at 10 lei and the other at 5 lei
            Assert.AreEqual(totalToPay, 15);
        }
    
    }
}
