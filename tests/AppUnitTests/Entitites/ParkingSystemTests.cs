using App.Entities;
using App.Exceptions;
using App.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppUnitTests
{
    [TestClass]
    public class ParkingSystemTests
    {
        private readonly ParkingSystem _parkingSystem;
        public ParkingSystemTests()
        {
            _parkingSystem = new ParkingSystem(new TimeProvider(), new BillingService(10, 5));
            _parkingSystem.TotalSpaces = 0;
        }

        [TestMethod]
        [ExpectedException(typeof(FullParkingLotException))]
        public void ShouldNotAllowCheckinWhenParkingLotFull()
        {
            _parkingSystem.CarCheckin(new Car("B01MTA"));
        }

        [TestMethod]
        public void ShouldNotCalculateBillForUnparkedCar()
        {
            Car car = new Car("B129MTA");
            var exception = Assert.ThrowsException<CarNotFoundException>(
                () => _parkingSystem.CalculateBill(car)
                );
            Assert.AreEqual(exception.Message, "Car with plate number B129MTA was not found in the parking lot.");
        }

        [TestMethod]
        public void ShouldNotAllowCheckOutForUnparkedCar()
        {
            Car car = new Car("B01BOB");
            var exception = Assert.ThrowsException<CarNotFoundException>(() =>
                    _parkingSystem.CarCheckout(car, 100)
            );
            Assert.AreEqual(exception.Message, "Car with plate number B01BOB was not found in the parking lot.");
        }

        [TestMethod]
        public void ShouldNotAllowCheckinForExistingCar()
        {
            Car car = new Car("B123BBB");
            _parkingSystem.TotalSpaces = 10;
            _parkingSystem.CarCheckin(car);
   
            Car car1 = new Car("B123BBB");
            var exception = Assert.ThrowsException<CarAlreadyCheckedInException>(() =>
                    _parkingSystem.CarCheckin(car1)
            );
            Assert.AreEqual(exception.Message, "Car with license plate B123BBB already exists in the parking lot.");
            _parkingSystem.TotalSpaces = 0;
        }

    }
}
